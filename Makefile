main: main.o 
	g++ -fopenmp main.o -o main
	
main.o: main.cpp input.hpp lbm.hpp
	g++ -c -fopenmp main.cpp

serial: main.cpp input.hpp lbm.hpp
	g++ main.cpp -o main
	
clean:
	rm -rf *.o main output *.csv *.tp *.tec *.png

run:
	rm -rf output
	mkdir output
	./main
