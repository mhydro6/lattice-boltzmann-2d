# Lattice Boltzmann 2D

Milo Feinberg

Code for solving the 2D Navier-Stokes equations using the Lattice Boltzmann method.  Developed for *Fluid Dynamics with the Lattice Boltzmann Method*, Prof. Karlin, Fall 2021 ETH Zurich.

### Usage

Parameters are passed to the simulation through the `input.hpp` file.  Once these are set:

1. compile the program:
    * Run in parallel with OpenMP -- `make`
    * Run in serial -- `make serial`
2. run the program: `make run`

The output files and plots will be found in the sub-directory `output`.  There are two types of output files:

1. `lbm_*.tec` -- contains field data.
2. `forces.csv` -- contains non-normalized force data on a specific boundary (only used in the cylinder case).

The following branches have the input file pre-configured for the test cases from exercise 2, 3, and 4.  They may be accessed by changing branches: `git branch <branch-name>`.

* `taylor-green` -- from exercise 2, simulation of a Taylor-Green vortex using the BGK collision operator.
* `shear-layer` -- from exercise 3, simulation of the roll up of a double shear layer using the KBC D collision operator.
* `cylinder` -- from exericse 4, simulation of the wake behind a 2D cylinder using the KBC D.collision operator.

### Dependencies

* GCC 9.3.0 or later
* GNU Make 4.2.1 or later
* OpenMP 5.0 or later (usually included with GCC)

### Results

#### Von Karman Street at Re=200
![](img/von_karman_street_velocity_field_RE100.png)

#### Double Periodic Shear Layer at Re=3000
![Double Shear layer at Reynolds Number = 3000](img/double_periodic_shearlayer_RE3000_velocity_mag.png)

#### Taylor Green Vortex at Re=10000
![](img/taylor_green_vortex_velocity_field.png)
