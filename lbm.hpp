/*================================== C++ ======================================*\
|                                                                               |
|                           2D Lattice-Boltzmann                                |
|                               Milo Feinberg                                   |
|                                                                               |
\*=============================================================================*/

#include <math.h>
#include <vector>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <omp.h>

class lbm
{
public:
    lbm(const int NptsX, const int NptsY, const double ReynoldsNumber, const double maxVelocity, const double characteristicLength):
        Nx(NptsX), 
        Ny(NptsY), 
        Re(ReynoldsNumber),
        Vmax(maxVelocity),
        Lchar(characteristicLength)
    {
        bufferSize = 1;                     // number of buffer cells (per edge)
        Np = 9;                             // number of populations
        realNx = Nx + 2*bufferSize;         // actual length of the arrays
        realNy = Ny + 2*bufferSize;         // actual length of the arrays
        realSize = realNx * realNy;         // length of the population vectors in memory
        Lx = (Nx-1.0)*1.0;                  // length of the domain in the X-direction
        Ly = (Nx-1.0)*1.0;                  // length of the domain in the Y-direction
        cs = 1.0 / std::sqrt(3);            // the speed of sound in lattice world
        nu  = Vmax * Lchar / Re;            // the kinematic viscosity
        mach = Vmax / cs;                   // estimated maximum mach number
        beta = 1/(1+(2*nu)/(cs*cs));        // relaxation coef.

        shift.resize(Np);                   // vector of shifts in memory for each population
        // Allocate memory for macro flow variables
        X.resize(realSize,   0.0);          // x-location of the nodes
        Y.resize(realSize,   0.0);          // y-location of the nodes
        rho.resize(realSize, 0.0);          // density
        Ux.resize(realSize,  0.0);          // x-velocity
        Uy.resize(realSize,  0.0);          // y-velocity
        nodeType.resize(realSize, 0);       // nodeType
        
        // Allocate memory for Populations
        f.resize(9);
        for (int p = 0; p < Np; p++) {
            f[p].resize(realSize, 0.0);
            shift[p] = cx[p] + realNx*cy[p];
        }
        
        initializeDomain();
        
        // WRITE TO TERMINAL
        std::cout << "LBM SOLVER SETTINGS" << std::endl;
        std::cout << "------------------------------------" << std::endl;
        std::cout << "Domain Size:               " << Nx << " x " << Ny << std::endl;
        std::cout << "Reynolds Number:           " << Re << std::endl;
        std::cout << " - Max Velocity:           " << Vmax << std::endl;
        std::cout << " - Characteristic Length:  " << Lchar << std::endl;
        std::cout << " - Kinematic Viscosity:    " << nu << std::endl;
        std::cout << "Est. Mach Number:          " << mach << std::endl;
        std::cout << "------------------------------------" << std::endl << std::endl;
        
    }

    void advect() 
    {
        /* loop through the populations */
#pragma omp parallel for
        for (size_t p = 0; p < Np; p++) {
            // loop through the population and shift values into the buffers
            if (shift[p] > 0) { // cases with a positive shift
                for (size_t k=realSize-1; k >= shift[p]; k--) {
                    f[p][k] = f[p][k-shift[p]];
                }
            } else if (shift[p] < 0) { //cases with negative shift
                for (size_t k=0; k < realSize + shift[p]; k++) {
                    f[p][k] = f[p][k-shift[p]];
                }
            }
            /* apply periodic boundary conditions */
            // x-direction
            if (cx[p] > 0) { // shift right buffer to left boundary nodes
                for (int j=0; j < realNy; j++) {
                    f[p][node(0,j-bufferSize)] = f[p][node(Nx,j-bufferSize)];
                }
            } else if (cx[p] < 0) { // shift left buffer to right boundary nodes
                for (int j=0; j < realNy; j++) {
                    f[p][node(Nx-bufferSize,j-bufferSize)] = f[p][node(-bufferSize,j-bufferSize)];
                }
            }
            // y-direction
            if (cy[p] > 0) { // shift top buffer to bottom boundary nodes
                for (int i=0; i < realNx; i++) {
                    f[p][node(i-bufferSize,0)] = f[p][node(i-bufferSize, Ny)];
                }
            } else if (cy[p] < 0) { // shift bottom buffer to top boundary nodes
                for (int i=0; i < realNx; i++) {
                        f[p][node(i-bufferSize,Ny-bufferSize)] = f[p][node(i-bufferSize,-(bufferSize))];
                }
            }
        }        
    }
    
    void applyEquilibriumBCs() 
    {
        for (int n=0; n < equilibriumBC.size(); n++) {
            int i, j;
            j = (equilibriumBC[n] / realNx) - 1;
            i =  equilibriumBC[n] - (j+1)*realNx - 1;

            // hard coded inlet variables.
            rho[node(i,j)] = 1.0;
            Ux[node(i,j)] = Vmax;
            Uy[node(i,j)] = 0.0;
            
            for (int p=0; p < Np; p++) {
                f[p][node(i,j)] = feq(p, node(i,j));
            }
        }
    }
    
    void applyNoBoundaryBCs() 
    {
        for (int n=0; n < noBoundaryBC.size(); n++) {
            int i, j;
            j = (noBoundaryBC[n] / realNx) - 1;
            i =  noBoundaryBC[n] - (j+1)*realNx - 1;
            
            for (int p=0; p < Np; p++) {
                if (nodeType[node(i+cx[p], j+cy[p])] == 0) {
                    f[p][node(i,j)] = f[p][node(i+cx[p], j+cy[p])];
                }
            }
        }
    }

    void applyBounceBackBCs() 
    {
        for (int n=0; n < bounceBackBC.size(); n++) {
            int i, j;
            j = (bounceBackBC[n] / realNx) - 1;
            i =  bounceBackBC[n] - (j+1)*realNx - 1;

            for (int p=0; p < Np; p++) {
                if (nodeType[node(i+cx[p], j+cy[p])] == 1 || nodeType[node(i+cx[p], j+cy[p])] == -1) {
                    f[pinv[p]][node(i,j)] = f[p][node(i+cx[p],j+cy[p])];
                }
            }
        }
    }
    
    void applyFreeSlipBCs()
    {
        for (int n=0; n < freeSlipBC.size(); n++) {
            int i, j;
            j = (freeSlipBC[n] / realNx) - 1;
            i =  freeSlipBC[n] - (j+1)*realNx - 1;

            for (int p=0; p < Np; p++) {
                if (nodeType[node(i+cx[p], j+cy[p])] == 0) {
                    std::swap(f[p][node(i,j)], f[pmir[p]][node(i-cx[p],j-cy[p])]);
                }
            }
        }
    }

    void collide_BGK()
    {
#pragma omp parallel for collapse(2)
        for (int i = 0; i < Nx; i++){
            for (int j = 0; j < Ny; j++){
                // Calculate new macro properties
                calcDensity(node(i,j));
                calcVelocityX(node(i,j));
                calcVelocityY(node(i,j));
                
                // Calculate new equilibrium populations
                for (int p = 0; p < Np; p++) {
                    f[p][node(i,j)] += 2.0*beta*(feq(p, node(i,j)) - f[p][node(i,j)]);
                }
            }
        }
    }

    void collide_KBCD()
    {
#pragma omp parallel for collapse(2)
        for (int i = 0; i < Nx; i++){
            for (int j = 0; j < Ny; j++) {            
                double PIxx, PIyy, PIxy, PIxx_eq, PIyy_eq, PIxy_eq, S, Seq, dsdh, dhdh, gamma;
                PIxx = PIyy = PIxy = PIxx_eq = PIyy_eq = PIxy_eq = S = Seq = dsdh = dhdh = gamma = 0.0;
                std::vector<double> feq_p(Np,  0.0);
                std::vector<double> deltaS(Np, 0.0);
                std::vector<double> deltaH(Np, 0.0);
                
                // Calculate conserved quantities properties
                calcDensity(node(i,j));
                calcVelocityX(node(i,j));
                calcVelocityY(node(i,j));
                
                // calculate the non-conserved higher-order moments                
                for (int p = 0; p < Np; p++) {
                    // at current state
                    PIxx += f[p][node(i,j)]*cx[p]*cx[p];
                    PIyy += f[p][node(i,j)]*cy[p]*cy[p];
                    PIxy += f[p][node(i,j)]*cx[p]*cy[p];
                    // at equilibrium
                    feq_p[p] = feq(p, node(i,j));
                    PIxx_eq += feq_p[p]*cx[p]*cx[p];
                    PIyy_eq += feq_p[p]*cy[p]*cy[p];
                    PIxy_eq += feq_p[p]*cx[p]*cy[p];
                }
                
                PIxx /= rho[node(i,j)];
                PIyy /= rho[node(i,j)];
                PIxy /= rho[node(i,j)];
                PIxx_eq /= rho[node(i,j)];
                PIyy_eq /= rho[node(i,j)];
                PIxy_eq /= rho[node(i,j)];
                
                // compute deltaS and deltaH
                for (int p = 0; p < Np; p++) {
                    // calculate s and seq
                    if (cx[p] == 0 && cy[p] == 0) {
                        S   = 0;
                        Seq = 0;
                    } else if (cy[p] == 0) {
                        S   = 0.25*rho[node(i,j)]*(PIxx - PIyy);
                        Seq = 0.25*rho[node(i,j)]*(PIxx_eq - PIyy_eq);
                    } else if (cx[p] == 0) {
                        S   = -0.25*rho[node(i,j)]*(PIxx - PIyy);
                        Seq = -0.25*rho[node(i,j)]*(PIxx_eq - PIyy_eq);
                    } else {
                        S   = 0.25*rho[node(i,j)]*cx[p]*cy[p]*PIxy;
                        Seq = 0.25*rho[node(i,j)]*cx[p]*cy[p]*PIxy_eq;
                    }
                    deltaS[p] = S - Seq;
                    deltaH[p] = f[p][node(i,j)] - feq_p[p] - deltaS[p];
                    dsdh += (deltaS[p] * deltaH[p]) / feq_p[p];
                    dhdh += (deltaH[p] * deltaH[p]) / feq_p[p];
                }
                
                // evaluate gamma
                if (dhdh < 1.0e-10) {
                    gamma = 2.0;
                } else {
                    gamma = (1/beta) - (2 - (1/beta)) * (dsdh / dhdh);
                }
                
                // update the populations
                for (int p = 0; p < Np; p++) {
                    f[p][node(i,j)] -= beta*(2*deltaS[p] + gamma*deltaH[p]);
                }
            }
        }
    }
        
    void calcForces(double arr[])
    {
        double FX = 0.0;
        double FY = 0.0;
        // loop through each node
        for (int n=0; n < bounceBackBC.size(); n++) {
            int i, j;
            j = (bounceBackBC[n] / realNx) - 1;
            i =  bounceBackBC[n] - (j+1)*realNx - 1;

            for (int p=0; p < Np; p++) {
                if (nodeType[node(i-cx[p], j-cy[p])] == 1) {
                    FX += -cx[p]*(f[pinv[p]][node(i,j)] + f[p][node(i+cx[p], j+cy[p])]);
                    FY += -cy[p]*(f[pinv[p]][node(i,j)] + f[p][node(i+cx[p], j+cy[p])]);
                }
            }
        }
        arr[0] = FX;
        arr[1] = FY;
    }
        
        
    void setInitialConditions(int initialConditions)
    {
        switch (initialConditions) {
            case 1: // steep gaussian profile.
                for (int i=0; i<Nx; i++) {
                    for (int j=0; j<Ny; j++) {
                        Ux[node(i,j)] = 0.1;
                        Uy[node(i,j)] = 0.25;
                        rho[node(i,j)] = 1.0 + 0.5*std::exp(-1000.0*std::pow((X[node(i,j)]/Lx - 0.25),2))*std::exp(-1000.0*std::pow((Y[node(i,j)]/Ly - 0.25),2));
                        
                        // Populations
                        for (int p = 0; p < Np; p++ ) {
                            f[p][node(i,j)] = feq(p, node(i,j));
                        }
                    }
                }
                break;
            case 2: // fake profiles for checking the advection steps
                for (int i=0; i<Nx; i++) {
                    for (int j=0; j<Ny; j++) {
                        Ux[node(i,j)] = 0.1;
                        Uy[node(i,j)] = 0.1;
                        rho[node(i,j)] = 1.0;
                        rho[node(2,2)] = 2.0;

                        // Populations
                        for (int p = 0; p < Np; p++ ) {
                            if (i == 5 && j == 3) {
                                f[p][node(i,j)] = 2;
                            } else {
                                f[p][node(i,j)] = 1;
                            }
                        }
                    }
                }
                break;
            case 3: // Taylor-Green vortex
            {
                double Kx = 2*M_PI/Lx;
                double Ky = 2*M_PI/Ly;
                double Ksqr = (Kx*Kx + Ky*Ky);
                
#pragma omp parallel for collapse(2)
                for (int i=0; i<Nx; i++) {
                    for (int j=0; j<Ny; j++) {
                        Ux[node(i,j)] = -Vmax*std::sqrt(Ky/Kx) * sin(Ky*Y[node(i,j)]) * cos(Kx*X[node(i,j)]);
                        Uy[node(i,j)] =  Vmax*std::sqrt(Kx/Ky) * sin(Kx*X[node(i,j)]) * cos(Ky*Y[node(i,j)]);
                        rho[node(i,j)] = 1 - (mach*mach)/(2*Ksqr) * (Ky*Ky*cos(2*Kx*X[node(i,j)]) + Kx*Kx*cos(2*Ky*Y[node(i,j)]));
                        
                        // Populations
                        for (int p = 0; p < Np; p++ ) {
                            f[p][node(i,j)] = feq(p, node(i,j));
                        }
                    }
                }
            }
            break;
            case 4: // Shear-Layer
            {
                double delta = 0.05;
                double kappa = 80;
#pragma omp parallel for collapse(2)
                for (int i=0; i<Nx; i++) {
                    for (int j=0; j<Ny; j++) {
                        rho[node(i,j)] = 1.0;
                        Uy[node(i,j)] = delta*Vmax*std::sin(2*M_PI*((i/Lx)+0.25));
                        if (j > (Ly/2)) {
                            Ux[node(i,j)] = Vmax*std::tanh(kappa*(0.75 - (j/Ly)));
                        } else {
                            Ux[node(i,j)] = Vmax*std::tanh(kappa*((j/Ly)-0.25));
                        }

                        // Populations
                        for (int p = 0; p < Np; p++ ) {
                            f[p][node(i,j)] = feq(p, node(i,j));
                        }
                    }
                }
            }
            break;
            case 5: // cylinder 
            {
                srand( (unsigned)time( NULL ) );
                double D = 20.0;      // circle diameter
                double Xc = 200.0;    // circle center x-coordinate
                double Yc = 200.0;    // circle center y-coordinate
                double Xbmin = 400.0; // random perturbation box coordinates
                double Xbmax = 750.0; // random perturbation box coordinates
                double Ybmin = 100.0; // random perturbation box y min
                double Ybmax = 300.0; // random perturbation box y maximum
                
                for (int i=0; i<Nx; i++) {
                    for (int j=0; j<Ny; j++) {
                        double Rsqr = (std::pow((X[node(i,j)] - Xc), 2) + std::pow((Y[node(i,j)] - Yc), 2));
                        if (Rsqr <= 0.25*D*D) { // inside the cylinder set values to NaN
                            rho[node(i,j)] = std::numeric_limits<double>::quiet_NaN();
                            Ux[node(i,j)] =  std::numeric_limits<double>::quiet_NaN();
                            Uy[node(i,j)] =  std::numeric_limits<double>::quiet_NaN();
                            nodeType[node(i,j)] = 1;
                        } else if (X[node(i,j)] > Xbmin && X[node(i,j)] < Xbmax && Y[node(i,j)] < Ybmax && Y[node(i,j)] > Ybmin) { // inside wake box seed Uy with random perturbations
                            rho[node(i,j)] = 1.0;
                            Ux[node(i,j)] = Vmax+0.1*Vmax*((double)rand()/RAND_MAX);
                            Uy[node(i,j)] = 0.25*Vmax*((double)rand()/RAND_MAX);
                        } else { // everywhere else in domain
                            rho[node(i,j)] = 1.0;
                            Ux[node(i,j)] = Vmax;
                            Uy[node(i,j)] = 0;
                        }
                                                
                        if (i == 0 ) {
                            nodeType[node(i,j)] = 2; // inlet
                            equilibriumBC.push_back(node(i,j));
                        } else if (i == Nx-1) {
                            nodeType[node(i,j)] = 3; // free slip top and bottom
                            noBoundaryBC.push_back(node(i,j));
                        }
                        
                        if (j == 0 || j == Ny-1) {
                            nodeType[node(i,j)] = 4;  // free slip top and bottom
                            freeSlipBC.push_back(node(i,j));
                        }
                        
                        // Populations
                        for (int p = 0; p < Np; p++ ) {
                            f[p][node(i,j)] = feq(p, node(i,j));
                        }
                    }
                }
                
                // find nodes bordering the cylinder
                for (int i=0; i<Nx; i++) {
                    for (int j=0; j<Ny; j++) {
                        int count = 0;
                        if (nodeType[node(i,j)] != 1) {
                            for (int p=1; p < Np; p++) {
                                int idx = i + cx[p];
                                int idy = j + cy[p];
                                if (nodeType[node(idx, idy)] == 1) {
                                    count += 1;
                                    break;
                                }
                            }
                            if (count > 0) {
                                nodeType[node(i,j)] = 5; // no-slip condition (bounce back)
                                bounceBackBC.push_back(node(i,j));
                            }
                        }
                    }
                }
            }
            break;
        }
    }
    
    void taylorGreeneSolution(int nTS)
    {
        double Kx = 2*M_PI/Lx;
        double Ky = 2*M_PI/Ly;
        double Ksqr = (Kx*Kx + Ky*Ky);

#pragma omp parallel for collapse(2)
        for (int i=0; i<Nx; i++) {
            for (int j=0; j<Ny; j++) {
                Ux[node(i,j)]  = -Vmax*std::sqrt(Ky/Kx) * std::exp(-nu*Ksqr*nTS) * sin(Ky*Y[node(i,j)]) * cos(Kx*X[node(i,j)]);
                Uy[node(i,j)]  =  Vmax*std::sqrt(Kx/Ky) * std::exp(-nu*Ksqr*nTS) * sin(Kx*X[node(i,j)]) * cos(Ky*Y[node(i,j)]);
                rho[node(i,j)] = 1 - ((mach*mach)/(2*Ksqr)) * (Ky*Ky*cos(2*Kx*X[node(i,j)]) + Kx*Kx*cos(2*Ky*Y[node(i,j)]));
                
                // Populations
                for (int p = 0; p < Np; p++ ) {
                    f[p][node(i,j)] = feq(p, node(i,j));
                }
            }
        }
    }
    
    void writeOut(int step )
    {
        std::ostringstream filename;
        filename << "output/lbm_" << std::setfill('0') << std::setw(5) << step << ".tec";
        std::ofstream outputfile;
        outputfile.open( filename.str().c_str() );
        outputfile << "TITLE=\"LB Simulation Output\"\n";
        outputfile << "VARIABLES = \"X\", \"Y\", \"TYPE\", \"RHO\", \"Ux\", \"Uy\"\n";
        outputfile << "ZONE T = \"DOMAIN\",I=" << Nx << ",J=" << Ny << ",F=POINT\n";
        for (int j = 0; j < Ny; j++) {
            for (int i = 0; i < Nx; i++) {
                outputfile  << X[node(i,j)] << ", " 
                            << Y[node(i,j)] << ", " 
                            << nodeType[node(i,j)] << ", " 
                            << std::scientific << rho[node(i,j)] << ", " 
                            << std::scientific << Ux[node(i,j)] << ", " 
                            << std::scientific << Uy[node(i,j)] << "\n";
            }
        }
    }
    
    void writeOutVerbose(int step )
    {
        std::ostringstream filename;
        filename << "output/lbm_" << std::setfill('0') << std::setw(5) << step << ".tec";
        std::ofstream outputfile;
        outputfile.open( filename.str().c_str() );
        outputfile << "TITLE=\"LB Simulation Output\"\n";
        outputfile << "VARIABLES = \"X\", \"Y\", \"TYPE\", \"RHO\", \"Ux\", \"Uy\", \"f0\", \"f1\", \"f2\", \"f3\", \"f4\", \"f5\", \"f6\", \"f7\", \"f8\"\n";
        outputfile << "ZONE T = \"DOMAIN\",I=" << Nx << ",J=" << Ny << ",F=POINT\n";
        for (int j = 0; j < Ny; j++) {
            for (int i = 0; i < Nx; i++) {
                outputfile  << X[node(i,j)] << ", " 
                            << Y[node(i,j)] << ", " 
                            << nodeType[node(i,j)] << ", " 
                            << std::scientific << rho[node(i,j)] << ", " 
                            << std::scientific << Ux[node(i,j)] << ", " 
                            << std::scientific << Uy[node(i,j)] << ", "
                            << std::scientific << f[0][node(i,j)] << ", "
                            << std::scientific << f[1][node(i,j)] << ", "
                            << std::scientific << f[2][node(i,j)] << ", "
                            << std::scientific << f[3][node(i,j)] << ", "
                            << std::scientific << f[4][node(i,j)] << ", "
                            << std::scientific << f[5][node(i,j)] << ", "
                            << std::scientific << f[6][node(i,j)] << ", "
                            << std::scientific << f[7][node(i,j)] << ", "
                            << std::scientific << f[8][node(i,j)] << "\n";
            }
        }
    }
    
    int node(int i, int j) 
    {
        // returns the index in memory based on the i, j indices in space
        return (i+1) + (j+1)*realNx;
    }

    
private:
    void initializeDomain() 
    {
        for (int i=-1; i < Nx+1; i++) {
            for (int j=-1; j < Ny+1; j++) {
                X[node(i,j)] = i;
                Y[node(i,j)] = j;
                if (i == -1 || i == Nx || j == -1 || j == Ny ) {
                    nodeType[node(i,j)] = -1;
                }
            }
        }
    }
    
    void calcDensity(int n) 
    {
        rho[n] = 0.0;
        for (int p = 0; p < Np; p++) {
            rho[n] += f[p][n];
        }
    }
    
    void calcVelocityX(int n) 
    {
        Ux[n] = 0.0;
        for (int p = 0; p < Np; p++) {
            Ux[n] += cx[p]*f[p][n];
        } 
        Ux[n] /= rho[n];
    }
    
    void calcVelocityY(int n) 
    {
        Uy[n] = 0.0;
        for (int p = 0; p < Np; p++) {
            Uy[n] += cy[p]*f[p][n];
        }
        Uy[n] /= rho[n];
    }
    
    double feq(int p, int n) 
    {
        // p is the population index, n is the node index in the various arrays 
        double phiX = (2.0 - std::sqrt(1 + 3.0*Ux[n]*Ux[n]))*std::pow(((2.0*Ux[n] 
                      + std::sqrt(1.0 + 3.0*Ux[n]*Ux[n]))/(1.0 - Ux[n])), cx[p]);
        double phiY = (2.0 - std::sqrt(1.0 + 3.0*Uy[n]*Uy[n]))*std::pow(((2.0*Uy[n] 
                      + std::sqrt(1.0 + 3.0*Uy[n]*Uy[n]))/(1.0 - Uy[n])), cy[p]);
        
        return rho[n] * W[p] * phiX * phiY;
    }
    
    double Lchar;                   // characteristic length used to calculate viscosity from Re
    double Lx;                      // length of the domain in the x-direction
    double Ly;                      // length of the domain in the y-direction
    double Re;                      // Reynolds number
    double Vmax;                    // estimated maximum velocity in domain
    double cs;                      // speed of sound in the fluid
    double nu;                      // kinematic viscosity
    double mach;                    // Mach number
    double beta;                    // beta parameter
    int Np;                         // number of populations
    int Nx;                         // number of grid points in the x-direction
    int Ny;                         // number of grid points in the y-direction
    int realNx;                     // number of grid points including buffer points in the x-direction
    int realNy;                     // number of grid points including buffer points in the y-direction
    int realSize;                   // total number of elements in the population vectors
    int bufferSize;                 // number of cells in the buffer regions
    std::vector<double> shift;      // vector containing the shifts for moving populations in memory
    std::vector<double> X;          // vector of x-locations of each node
    std::vector<double> Y;          // vector of y-locations of each node
    std::vector<double> rho;        // vector of the density at each node
    std::vector<double> Ux;         // vector of flow velocities in the x-direction at each node
    std::vector<double> Uy;         // vector of flow velocities in the y-direction at each node
    std::vector<int> nodeType;      // Type of node (for boundary condition implementation, etc.)
                                    // -1 -- buffer nodes
                                    //  0 -- fluid node (default)
                                    //  1 -- solid node (outside of flow domain)
                                    //  2 -- equilibrium BC node
                                    //  3 -- no-boundary BC node
                                    //  4 -- free slip BC node
                                    //  5 -- bounce-back BC node
    std::vector<int> equilibriumBC; // vector containing indices of inlet nodes (type 2)
    std::vector<int> noBoundaryBC;  // vector containing indices of outlet nodes (type 3)
    std::vector<int> freeSlipBC;    // vector containing indices of free slip nodes (type 4)
    std::vector<int> bounceBackBC;  // vector containing indices of bounce-back nodes  (type 5)
    // velocities and weights
    const std::vector<double> pinv{ 0, 3, 4, 1, 2, 8, 7, 6, 5};                                  
    const std::vector<double> pmir{ 0, 3, 4, 1, 2, 7, 8, 5, 6};                                  
    const std::vector<double>   cx{ 0, 1, 0,-1, 0, 1,-1, 1,-1};
    const std::vector<double>   cy{ 0, 0, 1, 0,-1, 1, 1,-1,-1};
    const std::vector<double> W{4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
    // population vector of vectors
    std::vector<std::vector<double>> f; // vector of population vectors
};
