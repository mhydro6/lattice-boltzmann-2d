/*================================== C++ ======================================*\
|                                                                               |
|                           2D Lattice-Boltzmann                                |
|                               Milo Feinberg                                   |
|                                                                               |
\*=============================================================================*/

//==============================================================================
//              USER INPUT DATA
//==============================================================================

// SIMULATION PARAMETERS
const bool WRITE = true;                // write out time-steps at writeInterval
const bool VERBOSE = false;             // write out time-steps at writeInterval

const int writeInterval = 200;          // number of time-steps between writeouts
const int nTS = 50000;                  // number of time-steps

const int ICs = 5;                      // 1 -- density gaussian
                                        // 2 -- advection checking stuff
                                        // 3 -- Taylor-Green Vortex
                                        // 4 -- shear layer
                                        // 5 -- cylinder
// DOMAIN PARAMETERS
const int Nx =  800;                    // number of elements in domain
const int Ny =  400;                    // number of elements in domain

// FLUID PARAMETERS
const double Re = 100;                  // the Reynolds Number
const double Vmax = 0.05;               // estimated maximum velocity in domain
const double Lchar = 20;                // characteristic length of the simulation

//==============================================================================
