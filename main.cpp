/*================================== C++ ======================================*\
|                                                                               |
|                           2D Lattice-Boltzmann                                |
|                               Milo Feinberg                                   |
|                                                                               |
\*=============================================================================*/

#include <iostream>
#include <cmath>
#include <algorithm>
#include "input.hpp"
#include "lbm.hpp"

int main()
{
    #ifdef _OPENMP
    std::cout << "Compiled with -fopenmp (OpenMP Standard: " << _OPENMP << ")" << std::endl;
    #else
    std::cout << "Compiled without -fopenmp" << std::endl;
    #endif
    
    // initialize the simulation
    lbm sim(Nx, Ny, Re, Vmax, Lchar);

    sim.setInitialConditions(ICs);
    
    // create vectors for storing the forces
    std::vector<double> Fx(nTS, 0.0);
    std::vector<double> Fy(nTS, 0.0);
    double forceArray[2];
    
    if (WRITE == true) {
        std::cout << "writing initial conditions" << std::endl;
        if (VERBOSE == true) {
            sim.writeOutVerbose(0);
        } else {
            sim.writeOut(0);
        }
    }
    
    for (int n = 1; n <= nTS; n++) {
        sim.advect();
        sim.applyNoBoundaryBCs();
        sim.applyEquilibriumBCs();
        sim.applyFreeSlipBCs();
        sim.applyBounceBackBCs();
        sim.calcForces(forceArray);
        sim.collide_KBCD();
        
        if (WRITE == true) {
            if (n % writeInterval == 0) {
                std::cout << "writing output at step: " << n << std::endl;
                if (VERBOSE == true) {
                    sim.writeOutVerbose(n);
                } else {
                    sim.writeOut(n);
                }
            }
        }
        
    Fx[n] = forceArray[0];
    Fy[n] = forceArray[1];
    }

    // write out the forces
    std::ostringstream filename;
    filename << "output/forces.csv";
    std::ofstream outputfile;
    outputfile.open( filename.str().c_str() );
    outputfile << "# FORCES ON CYLINDER\n";
    outputfile << "# time, FX, FY\n";
    for (int i=0; i < nTS; i++) {
        outputfile  << i << ", " 
                    << std::scientific << Fx[i] << ", " 
                    << std::scientific << Fy[i] << "\n";
    }
    
    return 0;
}
